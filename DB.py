import psycopg2
import os

#  TODO retrieve credentials as system variables for more security
DB_HOST = os.environ.get('RDS_ENDPOINT')
DB_USER = 'postgres'
DB_NAME = 'betfair'
DB_PASS = os.environ.get('RDS_PW')


def get_psql_conn():
    conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASS, host=DB_HOST)
    return conn


def create_bbo_table():
    query = "CREATE TABLE bbo (" \
            "bf_market_id TEXT," \
            "bf_selection_id INT," \
            "arrival_epochmillis NUMERIC," \
            "back_price_l1 NUMERIC," \
            "back_quantity_l1 NUMERIC," \
            "back_price_l2 NUMERIC," \
            "back_quantity_l2 NUMERIC," \
            "back_price_l3 NUMERIC," \
            "back_quantity_l3 NUMERIC," \
            "lay_price_l1 NUMERIC," \
            "lay_quantity_l1 NUMERIC," \
            "lay_price_l2 NUMERIC," \
            "lay_quantity_l2 NUMERIC," \
            "lay_price_l3 NUMERIC," \
            "lay_quantity_l3 NUMERIC," \
            "raw_data JSON," \
            "insertion_epochmillis NUMERIC," \
            "PRIMARY KEY (bf_market_id, bf_selection_id, arrival_epochmillis));"

    conn = get_psql_conn()
    cur = conn.cursor()

    cur.execute(query)
    conn.commit()
    cur.close()
    conn.close()


def create_orders_table():
    query = "CREATE TABLE orders (" \
            "bet_id BIGINT," \
            "bf_market_id TEXT," \
            "bf_selection_id INT," \
            "arrival_epochmillis NUMERIC," \
            "insertion_epochmillis NUMERIC," \
            "side TEXT," \
            "matched_price NUMERIC," \
            "matched_size NUMERIC," \
            "raw_data JSON," \
            "PRIMARY KEY (bet_id, bf_market_id, bf_selection_id, arrival_epochmillis));"

    conn = get_psql_conn()
    cur = conn.cursor()
    cur.execute(query)
    conn.commit()
    cur.close()
    conn.close()



def create_betfair_orders_table():
    query = "CREATE TABLE betfair_orders (" \
            "bet_id BIGINT," \
            "ref TEXT," \
            "market_id TEXT," \
            "selection_id INT," \
            "pnl NUMERIC," \
            "expected_pnl NUMERIC," \
            "raw_data JSON," \
            "settled_timestamp INT," \
            "PRIMARY KEY (bet_id));"

    conn = get_psql_conn()
    cur = conn.cursor()
    cur.execute(query)

    conn.commit()
    cur.close()
    conn.close()


def create_runners_table():
    query = "CREATE TABLE runners (" \
            "bf_market_id TEXT," \
            "bf_selection_id INT," \
            "bf_name TEXT," \
            "bsp NUMERIC," \
            "result TEXT," \
            "PRIMARY KEY (bf_market_id, bf_selection_id));"

    conn = get_psql_conn()
    cur = conn.cursor()
    cur.execute(query)
    conn.commit()

    cur.close()
    conn.close()


def create_strategies_table():
    query = "CREATE TABLE strategies (" \
            "bf_market_id TEXT," \
            "bf_selection_id INT," \
            "strategy_ref TEXT," \
            "status TEXT," \
            "pnl NUMERIC," \
            "expected_pnl NUMERIC," \
            "creation_timestamp NUMERIC," \
            "backed_quantity NUMERIC," \
            "backed_average_odds NUMERIC," \
            "layed_average_odds NUMERIC," \
            "layed_quantity NUMERIC," \
            "PRIMARY KEY (bf_market_id, bf_selection_id, strategy_ref));"

    conn = get_psql_conn()
    cur = conn.cursor()
    cur.execute(query)
    conn.commit()

    cur.close()
    conn.close()

def create_markets_table():
    query = "CREATE TABLE markets (" \
            "bf_market_id TEXT," \
            "start_timestamp INT," \
            "market_type TEXT," \
            "event_type TEXT," \
            "country TEXT," \
            "raw_data JSON," \
            "PRIMARY KEY (bf_market_id));"

    conn = get_psql_conn()
    cur = conn.cursor()
    cur.execute(query)
    conn.commit()

    cur.close()
    conn.close()